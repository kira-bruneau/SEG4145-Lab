/*******************************************************************************
 *
 * Names:       Victor Diec (6901506), Kurt Bruneau (7238982)
 * Course Code: SEG 4145
 * Lab Number:  3
 * Date:        Mar 9, 2017
 *
 * Description:
 * Simple movement and collision detection
 *
 ******************************************************************************/

#include "robot.h"

const Command fix_path[] = {
  { MOVE,   -1  },
  { ROTATE,  90 },
  { MOVE,    2  },
  { ROTATE, -90 }
};

void setup() {
  initAll();
  printStudentNums("6901506", "7238982");
}

void loop() {
  if (moveAndCheck(1, 10)) {
    displayAmbTemp();
    traverse(fix_path, ARRAY_LEN(fix_path));
  } else {
    pause(1);
  }
}
