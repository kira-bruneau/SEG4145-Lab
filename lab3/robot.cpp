#include <Arduino.h>
#include <SoftwareSerial.h>
#include <Wire.h>

#include "robot.h"

/* Thermometer constants */
#define TEMP_SENSOR 0x68

/* Sonar constants */
#define CENTER_SENSOR_PIN 22

/* Motor pin constants */
#define LEFT_MOTOR_PIN 45
#define RIGHT_MOTOR_PIN 8
#define LEFT_SENSOR_PIN 48
#define RIGHT_SENSOR_PIN 49

/* Motor movement constants */
#define LEFT_MOTOR_FORWARD 245
#define RIGHT_MOTOR_FORWARD 138
#define LEFT_MOTOR_BACKWARD 160
#define RIGHT_MOTOR_BACKWARD 233
#define MOTOR_MILLIS_PER_TILE 10000.0/7.3
#define NUM_TICKS_PER_TILE 113

/* Motor rotation constants */
#define MOTOR_MILLIS_PER_DEGREE_CW 3000.0/360.0
#define MOTOR_MILLIS_PER_DEGREE_CCW 2900.0/360.0

/* Initialize the LCD */
SoftwareSerial lcd(0, 18);

void initAll() {
  initSerial();
  initLED();
  initLCD();
  initThermometer();
  initMotors();
  initMotorSensors();
}

void initSerial() {
  Serial.begin(9600);
}

void initLED() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void initLCD() {
  lcd.begin(9600);
}

void initThermometer() {
  Wire.begin();
}

void initMotors() {
  pinMode(LEFT_MOTOR_PIN, OUTPUT);
  pinMode(RIGHT_MOTOR_PIN, OUTPUT);
}

void initMotorSensors() {
  pinMode(LEFT_SENSOR_PIN, INPUT);
  pinMode(RIGHT_SENSOR_PIN, INPUT);
}

void blink(unsigned int seconds) {
  for (unsigned int i = 0; i < seconds; i++) {
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
  }

  digitalWrite(LED_BUILTIN, LOW);
}

/**
 * Execute an LCD command
 */
static void lcdCommand(char command) {
  lcd.write(0xFE);
  lcd.write(command);
}

void clearScreen() {
  lcdCommand(0x01);
}

/**
 * Positions the cursor for the LCD
 */
static void setCursor(char col, char row) {
  lcdCommand(col + row * 64 + 128);
}

void printCentered(const char *first, const char *second) {
  clearScreen();
  setCursor((16 - strlen(first)) / 2, 0);
  lcd.print(first);
  setCursor((16 - strlen(second)) / 2, 1);
  lcd.print(second);
}

void printStudentNums(const char *n1, const char *n2) {
  printCentered(n1, n2);
  blink(5);
  clearScreen();
}

int getAmbTemp() {
  Wire.beginTransmission(TEMP_SENSOR);
  Wire.write(0x01);
  Wire.endTransmission();
  Wire.requestFrom(TEMP_SENSOR, 1); // Request data
  while (Wire.available() < 1); // Wait for data
  return Wire.read();
}

void displayAmbTemp() {
  char degrees[13];
  sprintf(degrees, "%d degrees", getAmbTemp());

  // Print centered "Temperature:" in top, "%d degrees", temp in bottom
  printCentered("Temperature:", degrees);
  blink(5);
  clearScreen();
}

bool detectObstruction(unsigned int limit) {
  pinMode(CENTER_SENSOR_PIN, OUTPUT);
  digitalWrite(CENTER_SENSOR_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(CENTER_SENSOR_PIN, HIGH);
  delayMicroseconds(5);
  digitalWrite(CENTER_SENSOR_PIN, LOW);
  pinMode(CENTER_SENSOR_PIN, INPUT);
  unsigned long duration = pulseIn(CENTER_SENSOR_PIN, HIGH);
  unsigned long distance = duration / 58;
  Serial.print(duration);
  Serial.print(" micro-seconds\t");

  //Approximately 6 cm from sensor to the front of the end robot
  //So if we want to see anything within 10cm of the front of the robot, we have to see if there's anything within 16 cm of the sensor

  //A calculator with height of 7 cm can only be detected within 18 cm (from front of the robot, not the sensor)
  //There's a limit to how much we can scan out far ahead

  Serial.print(distance);
  Serial.println(" cm");

  //When the robot detects the obstruction, it will react here
  return distance <= limit + 6;
}

void move(float tiles) {
  printCentered("Moving", tiles >= 0 ? "Forward" : "Backward");
  analogWrite(LEFT_MOTOR_PIN, tiles >= 0 ? LEFT_MOTOR_FORWARD : LEFT_MOTOR_BACKWARD);
  analogWrite(RIGHT_MOTOR_PIN, tiles >= 0 ? RIGHT_MOTOR_FORWARD : RIGHT_MOTOR_BACKWARD);

  delay(MOTOR_MILLIS_PER_TILE * fabs(tiles));

  clearScreen();
  analogWrite(LEFT_MOTOR_PIN, 0);
  analogWrite(RIGHT_MOTOR_PIN, 0);
}

bool moveAndCheck(int tiles, unsigned int distance) {
  printCentered("Moving", "Forward");
  analogWrite(LEFT_MOTOR_PIN, LEFT_MOTOR_FORWARD);
  analogWrite(RIGHT_MOTOR_PIN, RIGHT_MOTOR_FORWARD);

  int count = 0;
  int limit = tiles * NUM_TICKS_PER_TILE / 7;
  bool state = digitalRead(LEFT_SENSOR_PIN);
  bool obstruction = false;
  while (count < limit) {
    if (state != digitalRead(LEFT_SENSOR_PIN)) {
      count++;
      state = !state;
    }
    
    if (detectObstruction(distance)) {
      obstruction = true;
      break;
    }
  }

  clearScreen();
  analogWrite(RIGHT_MOTOR_PIN, 0);
  analogWrite(LEFT_MOTOR_PIN, 0);
  
  return obstruction;
}

void rotate(float degrees) {
  printCentered("Rotating", degrees >= 0 ? "Right" : "Left");
  analogWrite(LEFT_MOTOR_PIN, degrees >= 0 ? LEFT_MOTOR_FORWARD : LEFT_MOTOR_BACKWARD);
  analogWrite(RIGHT_MOTOR_PIN, degrees >= 0 ? RIGHT_MOTOR_BACKWARD : RIGHT_MOTOR_FORWARD);

  if (degrees >= 0) {
    delay(MOTOR_MILLIS_PER_DEGREE_CW * degrees);
  } else {
    delay(MOTOR_MILLIS_PER_DEGREE_CCW * -degrees);
  }

  clearScreen();
  analogWrite(LEFT_MOTOR_PIN, 0);
  analogWrite(RIGHT_MOTOR_PIN, 0);
}

void pause(float seconds) {
  printCentered("Stopped", "");
  delay(seconds * 1000);
  clearScreen();
}

void traverse(const Command *path, size_t len) {
  for (size_t i = 0; i < len; i++) {
    switch (path[i].type) {
    case MOVE: move(path[i].val); break;
    case ROTATE: rotate(path[i].val); break;
    case PAUSE: pause(path[i].val); break;
    }
  }
}

void traverse_backwards(const Command *path, size_t len) {
  for (size_t i = len; i-- != 0 ;) {
    switch (path[i].type) {
    case MOVE: move(-path[i].val); break;
    case ROTATE: rotate(-path[i].val); break;
    case PAUSE: pause(path[i].val); break;
    }
  }
}
