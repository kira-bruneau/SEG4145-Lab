/*******************************************************************************
 *
 * Names:       Victor Diec (6901506), Kurt Bruneau (7238982)
 * Course Code: SEG 4145
 * Lab Number:  2
 * Date:        Feb 15, 2016
 *
 * Description:
 * A program that walks along a given path
 *
 ******************************************************************************/

#include <Arduino.h>
#include "robot.h"

#define ARRAY_LEN(x) (sizeof(x)/sizeof(*x))

/* Motor pin constants */
#define LEFT_MOTOR_PIN 45
#define RIGHT_MOTOR_PIN 8
#define LEFT_SENSOR_PIN 48
#define RIGHT_SENSOR_PIN 49

/* Motor speed constants */
#define LEFT_MOTOR_FORWARD 210
#define RIGHT_MOTOR_FORWARD 138
#define LEFT_MOTOR_BACKWARD 80
#define RIGHT_MOTOR_BACKWARD 245

/* Motor scaling constants */
#define MOTOR_MILLIS_PER_TILE 4000/3
#define MOTOR_MILLIS_PER_DEGREE ((float)2950/(float)360)

const Command path[] = {
  { MOVE,    2       },
  { ROTATE,  90      },
  { MOVE,    2       },
  { ROTATE,  90      },
  { MOVE,    3       },
  { ROTATE,  90      },
  { MOVE,    3       },
  { ROTATE,  90      },
  { MOVE,    2       },
  { ROTATE, -45      },
  { MOVE,    sqrt(2) },
  { ROTATE, -135     },
  { MOVE,    4       },
  { ROTATE, -90      },
  { MOVE,    2       },
  { ROTATE, -90      },
  { MOVE,    2       }
};

void setup() {
  initLCD();
  initLED();
  initMotors();

  Serial.begin(9600);

  analogWrite(LEFT_MOTOR_PIN, LEFT_MOTOR_FORWARD);
  analogWrite(RIGHT_MOTOR_PIN, RIGHT_MOTOR_FORWARD);

  int leftCount = 0;
  int rightCount = 0;
  bool leftState = digitalRead(LEFT_SENSOR_PIN);
  bool rightState = digitalRead(RIGHT_SENSOR_PIN);

  while (leftCount < 10000 && rightCount < 10000) {
    if (leftState != digitalRead(LEFT_SENSOR_PIN)) {
      leftCount++;
      leftState = !leftState;

      // char buf[11];
      // sprintf(buf, "%d %d", leftCount, rightCount);
      // Serial.println(buf);
    }

    if (rightState != digitalRead(RIGHT_SENSOR_PIN)) {
      rightCount++;
      rightState = !rightState;

      // char buf[11];
      // sprintf(buf, "%d %d", leftCount, rightCount);
      // Serial.println(buf);
    }

    if (leftCount > 300)
      {
        float threshold = (float)leftCount / (float)rightCount - 0.7;
        if (threshold > 0.01) {
          analogWrite(LEFT_MOTOR_PIN, 0);
          analogWrite(RIGHT_MOTOR_PIN, RIGHT_MOTOR_FORWARD);
        } else if (threshold < -0.01) {
          analogWrite(LEFT_MOTOR_PIN, LEFT_MOTOR_FORWARD);
          analogWrite(RIGHT_MOTOR_PIN, 0);
        } else {
          analogWrite(LEFT_MOTOR_PIN, LEFT_MOTOR_FORWARD);
          analogWrite(RIGHT_MOTOR_PIN, RIGHT_MOTOR_FORWARD);
        }
      }

  }

  analogWrite(LEFT_MOTOR_PIN, 0);
  analogWrite(RIGHT_MOTOR_PIN, 0);

  while (true) {
  }
}

void loop() {
  // printStudentNums("6901506", "7238982");

  printCentered("Path", "Forward");
  delay(3000);
  traverse(path, ARRAY_LEN((path)));
  stop(5);

  printCentered("Path", "Backward");
  delay(3000);
  traverse_backwards(path, ARRAY_LEN(path));
  stop(5);
}
