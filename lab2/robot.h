#ifndef ROBOT_H
#define ROBOT_H

#include <stddef.h>

enum CommandType {
  MOVE,
  ROTATE,
  STOP
};

struct Command {
  CommandType type;
  float val;
};

/**
 * Initializes the robot's LCD
 */
void initLCD();

/**
 * Initializes the robot's LED
 */
void initLED();

/**
 * Initializes the robot's motors
 */
void initMotors();

/**
 * Clears the LCD screen
 */
void clearScreen();

/**
 * Prints two strings centered to the LCD
 */
void printCentered(const char *first, const char *second);

/**
 * Prints the two student numbers for 5 seconds
 * and blinks the LED every second
 */
void printStudentNums(const char *n1, const char *n2);

/**
 * Move forward / backward
 *
 * @tiles Number of tiles to move for
 */
void move(float tiles);

/**
 * Rotate clock-wise / counter-clock-wise
 *
 * @param degrees Angle of rotation in degrees
 */
void rotate(float degrees);

/**
 * Stop all motors
 *
 * @param ms Number of seconds to stay stopped for
 */
void stop(float seconds);

/**
 * Execute a series of commands
 *
 * @param path The list of commands to execute
 * @param len The number of commands to execute
 */
void traverse(const Command *path, size_t len);

/**
 * Execute a series of commands in reverse
 *
 * @param path The list of commands to execute
 * @param len The number of commands to execute
 */
void traverse_backwards(const Command *path, size_t len);

#endif
