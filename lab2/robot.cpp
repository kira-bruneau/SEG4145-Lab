#include <Arduino.h>
#include <SoftwareSerial.h>
#include "robot.h"

/* Motor pins */
#define LEFT_MOTOR_PIN 45
#define RIGHT_MOTOR_PIN 8

/* Motor movement constants */
#define LEFT_MOTOR_FORWARD 245
#define RIGHT_MOTOR_FORWARD 138
#define LEFT_MOTOR_BACKWARD 160
#define RIGHT_MOTOR_BACKWARD 233
#define MOTOR_MILLIS_PER_TILE 10000.0/7.3

/* Motor rotation constants */
#define MOTOR_MILLIS_PER_DEGREE_CW 3000.0/360.0
#define MOTOR_MILLIS_PER_DEGREE_CCW 2900.0/360.0

/* Initialize the LCD */
SoftwareSerial lcd(0, 18);

void initLCD() {
  lcd.begin(9600);
}

void initLED() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void initMotors() {
  pinMode(LEFT_MOTOR_PIN, OUTPUT);
  pinMode(RIGHT_MOTOR_PIN, OUTPUT);
}

/**
 * Execute an LCD command
 */
static void lcdCommand(char command) {
  lcd.write(0xFE);
  lcd.write(command);
}

void clearScreen() {
  lcdCommand(0x01);
}

/**
 * Positions the cursor for the LCD
 */
static void setCursor(char col, char row) {
  lcdCommand(col + row * 64 + 128);
}

void printCentered(const char *first, const char *second) {
  clearScreen();
  setCursor((16 - strlen(first)) / 2, 0);
  lcd.print(first);
  setCursor((16 - strlen(second)) / 2, 1);
  lcd.print(second);
}

void printStudentNums(const char *n1, const char *n2) {
  printCentered(n1, n2);

  // LED light blinking
  for (int i = 0; i < 5; i++) {
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
  }

  digitalWrite(LED_BUILTIN, LOW);
  clearScreen();
}

void move(float tiles) {
  printCentered("Moving", tiles >= 0 ? "Forward" : "Backward");
  analogWrite(LEFT_MOTOR_PIN, tiles >= 0 ? LEFT_MOTOR_FORWARD : LEFT_MOTOR_BACKWARD);
  analogWrite(RIGHT_MOTOR_PIN, tiles >= 0 ? RIGHT_MOTOR_FORWARD : RIGHT_MOTOR_BACKWARD);

  delay(MOTOR_MILLIS_PER_TILE * fabs(tiles));

  clearScreen();
  analogWrite(LEFT_MOTOR_PIN, 0);
  analogWrite(RIGHT_MOTOR_PIN, 0);
}

void rotate(float degrees) {
  printCentered("Rotating", degrees >= 0 ? "Right" : "Left");
  analogWrite(LEFT_MOTOR_PIN, degrees >= 0 ? LEFT_MOTOR_FORWARD : LEFT_MOTOR_BACKWARD);
  analogWrite(RIGHT_MOTOR_PIN, degrees >= 0 ? RIGHT_MOTOR_BACKWARD : RIGHT_MOTOR_FORWARD);

  if (degrees >= 0) {
    delay(MOTOR_MILLIS_PER_DEGREE_CW * degrees);
  } else {
    delay(MOTOR_MILLIS_PER_DEGREE_CCW * -degrees);
  }

  clearScreen();
  analogWrite(LEFT_MOTOR_PIN, 0);
  analogWrite(RIGHT_MOTOR_PIN, 0);
}

void stop(float seconds) {
  printCentered("Stopped", "");
  delay(seconds * 1000);
  clearScreen();
}

void traverse(const Command *path, size_t len) {
  for (size_t i = 0; i < len; i++) {
    switch (path[i].type) {
    case MOVE: move(path[i].val); break;
    case ROTATE: rotate(path[i].val); break;
    case STOP: stop(path[i].val); break;
    }
  }
}

void traverse_backwards(const Command *path, size_t len) {
  for (size_t i = len; i-- != 0 ;) {
    switch (path[i].type) {
    case MOVE: move(-path[i].val); break;
    case ROTATE: rotate(-path[i].val); break;
    case STOP: stop(path[i].val); break;
    }
  }
}
