(function(window, document) {
    const controls = {
        "left45": {"Left": 32},
        "forward": {"Forward": 10},
        "right45": {"Right": 32},
        "left90": {"Left": 64},
        "right90": {"Right": 64},
        "left135": {"Left": 96},
        "backward": {"Backward": 10},
        "right135": {"Right": 96}
    };

    const keycodeControl = {
        'KeyW': "left45",
        'KeyE': "forward",
        'KeyR': "right45",
        'KeyS': "left90",
        'KeyF': "right90",
        'KeyA': "left135",
        'KeyD': "backward",
        'KeyG': "right135",
    };

    const socket = new WebSocket("ws://127.0.0.1:3000");
    socket.onopen = function(e) {
        for (let [id, action] of Object.entries(controls)) {
            const el = document.getElementById(id);
            el.addEventListener('click', () => {
                socket.send(JSON.stringify(controls[id]));
            }, false);
        }
    };

    window.addEventListener('keydown', (e) => {
        const id = keycodeControl[e.code];
        if (!id) return;
        const el = document.getElementById(id);
        el.classList.add('active');
        el.click();
    }, false);

    window.addEventListener('keyup', (e) => {
        const id = keycodeControl[e.code];
        if (!id) return;
        const el = document.getElementById(id);
        el.classList.remove('active');
    }, false);

    socket.onmessage = function(e) {
        console.log(JSON.parse(e.data));
        console.log('Update distances & temperature');
    };

    socket.onerror = function(e) {
        alert("Failed to connect to server");
    };
})(window, document)
