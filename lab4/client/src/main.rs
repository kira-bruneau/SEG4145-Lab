extern crate futures;
extern crate tokio_core;
extern crate tokio_io;
extern crate tokio_file_unix;
extern crate bytes;
extern crate ws;

#[macro_use]
extern crate serde_derive;
extern crate serde_json;

mod input;
mod proto;

use std::{env, io};
use std::str;
use std::net::SocketAddr;
use std::thread;

use futures::{future, Future, Stream};
use futures::sync::mpsc;
use tokio_core::reactor::Core;
use tokio_core::net::TcpStream;
use tokio_io::AsyncRead;
use tokio_file_unix::{File, StdFile};

use input::{Input, DelimCodec};
use proto::{RobotCodec};

fn main() {
    let mut args = env::args().skip(1);

    let addr = args.next()
        .map(|arg| arg.parse::<SocketAddr>().expect("Invalid address"))
        .expect("Missing server address");

    let ws_port = args.next()
        .map(|arg| arg.parse::<u16>().expect("Invalid port"))
        .unwrap_or(3000);

    // Initialize event loop
    let mut core = Core::new().unwrap();
    let handle = core.handle();

    // Setup stdin for non blocking I/O (unix only for now)
    let stdin = io::stdin();
    let stdin = StdFile(stdin.lock());
    let stdin = File::new_nb(stdin).unwrap()
        .into_io(&handle).unwrap()
        .framed(DelimCodec(b'\n'));

    // Setup websocket server
    let (ws_tx, ws_rx) = mpsc::unbounded();
    thread::spawn(move || {
        ws::listen(("127.0.0.1", ws_port), |_out| {
            |msg: ws::Message| {
                ws_tx.send(msg.into_text().unwrap()).unwrap();
                Ok(())
            }
        }).unwrap();
    });

    // Setup client
    let client = TcpStream::connect(&addr, &handle).and_then(|stream| {
        println!("Connected to {}", stream.peer_addr().unwrap());
        let (sink, stream) = stream.framed(RobotCodec::new()).split();

        // Process input from stdin
        let input = stdin

            // Convert lines from stdin into inputs
            .map(|line| {
                let line = str::from_utf8(&line)
                    .map_err(|_| input::ParseError::InvalidUtf8)?;

                line.parse::<Input>()
            })

            // Unpack and display errors
            .filter_map(|input| match input {
                Err(err) => {
                    println!("{:?}", err);
                    None
                },
                Ok(input) => Some(input)
            })

            // Close stream after receiving Input::Quit
            .take_while(|input| future::ok(match input {
                &Input::Quit => false,
                _ => true
            }))

            // Unpack requests from input
            .filter_map(|input| match input {
                Input::Request(req) => Some(req),
                _ => None,
            })

            .select(ws_rx
                    .map_err(|_| panic!()) // ws_rx has no errors
                    .map(|json| {
                        serde_json::from_str(&json).unwrap()
                    }))

            // Forward stream of requests to sink
            .forward(sink)
            .map(|_| ());

        // Process responses from stream
        let output = stream

            // Display responses
            .map(|response| {
                println!("{:?}", response);
            })

            // Wait for connection to close
            .for_each(|_| Ok(()))
            .and_then(|_| {
                println!("Connection closed by server");
                Ok(())
            })
            .or_else(|err| {
                println!("{}", err);
                Ok(())
            });

        // Wait for either input or output to complete
        Future::select(input, output).then(|_| Ok(()))
    });

    // Start event loop
    core.run(client).unwrap();
}
