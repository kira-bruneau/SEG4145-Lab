use std::io;
use std::collections::VecDeque;

use tokio_io::codec::{Decoder, Encoder};
use bytes::BytesMut;

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub enum Request {
    Forward(u8),
    Backward(u8),
    Right(u8),
    Left(u8),
    Distance,
    Temperature,
}

#[derive(Clone, PartialEq, Debug)]
pub enum Response {
    Distance(u16),
    Temperature(Vec<u8>),
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum RequestId {
    Distance,
    Temperature,
}

#[derive(Clone, Default, Debug)]
pub struct RobotCodec {
    requests: VecDeque<RequestId>,
}

impl RobotCodec {
    pub fn new() -> Self {
        RobotCodec { ..Default::default() }
    }
}

impl Decoder for RobotCodec {
    type Item = Response;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> io::Result<Option<Self::Item>> {
        if src.is_empty() {
            return Ok(None);
        }

        if let Some(&request_id) = self.requests.front() {
            match request_id {
                RequestId::Distance => {
                    if src.len() >= 2 {
                        self.requests.pop_front();
                        let dist = src.split_to(2);
                        Ok(Some(Response::Distance(dist[0] as u16 | dist[1] as u16 >> 8)))
                    } else {
                        Ok(None)
                    }
                },
                RequestId::Temperature => {
                    if src.len() >= 9 {
                        self.requests.pop_front();
                        Ok(Some(Response::Temperature(src.split_to(9).to_vec())))
                    } else {
                        Ok(None)
                    }
                },
            }
        } else {
            Err(io::Error::new(io::ErrorKind::InvalidInput,
                               "Unexpected response from server"))
        }
    }
}

impl Encoder for RobotCodec {
    type Item = Request;
    type Error = io::Error;

    fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> io::Result<()> {
        match item {
            Request::Forward(dist) => dst.extend(&[0, dist]),
            Request::Backward(dist) => dst.extend(&[1, dist]),
            Request::Right(deg) => dst.extend(&[2, deg]),
            Request::Left(deg) => dst.extend(&[3, deg]),
            Request::Distance => {
                self.requests.push_back(RequestId::Distance);
                dst.extend(&[4])
            },
            Request::Temperature => {
                self.requests.push_back(RequestId::Temperature);
                dst.extend(&[5])
            },
        };
        Ok(())
    }
}
