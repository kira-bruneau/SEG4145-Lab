use std::io;
use std::str::FromStr;

use tokio_io::codec::{Decoder, Encoder};
use bytes::BytesMut;

use proto;

#[derive(Clone, PartialEq, Debug)]
pub enum Input {
    Request(proto::Request),
    Quit,
}

#[derive(Clone, PartialEq, Debug)]
pub enum ParseError {
    Empty,
    InvalidUtf8,
    InvalidCommand,
    InvalidArgument,
}

impl FromStr for Input {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Input, ParseError> {
        let mut args = s.split_whitespace();
        if let Some(cmd) = args.next() {
            match cmd {
                "1" | "f" | "forward" => {
                    let dist = args.next().map_or(Ok(255), parse_dist)?;
                    Ok(Input::Request(proto::Request::Forward(dist)))
                },
                "2" | "b" | "backward" => {
                    let dist = args.next().map_or(Ok(255), parse_dist)?;
                    Ok(Input::Request(proto::Request::Backward(dist)))
                },
                "3" | "r" | "right" => {
                    let angle = args.next().map_or(Ok(32), parse_angle)?;
                    Ok(Input::Request(proto::Request::Right(angle)))
                },
                "4" | "l" | "left" => {
                    let angle = args.next().map_or(Ok(32), parse_angle)?;
                    Ok(Input::Request(proto::Request::Left(angle)))
                },
                "5" | "d" | "distance" => Ok(Input::Request(proto::Request::Distance)),
                "6" | "t" | "temperature" => Ok(Input::Request(proto::Request::Temperature)),
                "7" | "q" | "quit" => Ok(Input::Quit),
                _ => Err(ParseError::InvalidCommand),
            }
        } else {
            Err(ParseError::Empty)
        }
    }
}

/// Parses a robot distance value from a string
///
/// Range: 0cm - 20cm
fn parse_dist(s: &str) -> Result<u8, ParseError> {
    if let Ok(dist) = s.parse::<u8>() {
        if dist <= 20 {
            Ok(dist)
        } else {
            Err(ParseError::InvalidArgument)
        }
    } else {
        Err(ParseError::InvalidArgument)
    }
}

/// Parses a robot angle value from a string
///
/// Range: 0 deg - 359 deg
fn parse_angle(s: &str) -> Result<u8, ParseError> {
    if let Ok(angle) = s.parse::<u16>() {
        if angle < 360 {
            Ok((angle as u32 * 256 / 360) as u8)
        } else {
            Err(ParseError::InvalidArgument)
        }
    } else {
        Err(ParseError::InvalidArgument)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct DelimCodec<D>(pub D);

impl<D: Into<u8> + Copy> Decoder for DelimCodec<D> {
    type Item = Vec<u8>;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> io::Result<Option<Self::Item>> {
        Ok(src.iter().position(|b| *b == self.0.into())
           .map(|n| src.split_to(n + 1).to_vec()))
    }

    fn decode_eof(&mut self, src: &mut BytesMut) -> io::Result<Option<Self::Item>> {
        if src.is_empty() {
            Ok(None)
        } else {
            Ok(Some(src.take().to_vec()))
        }
    }
}

impl <D: Into<u8> + Copy> Encoder for DelimCodec<D> {
    type Item = Vec<u8>;
    type Error = io::Error;

    fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> io::Result<()> {
        dst.extend(item);
        dst.extend(&[self.0.into()]);
        Ok(())
    }
}
