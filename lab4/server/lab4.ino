#include <Wirefree.h>
#include <WifiServer.h>

#include "robot.h"

WIFI_PROFILE wireless_prof = {
                        /* SSID */ "stingray",
         /* WPA/WPA2 passphrase */ "12345678",
                  /* IP address */ "192.168.1.157",
                 /* subnet mask */ "255.255.255.0",
                  /* Gateway IP */ "192.168.1.1", };

// Setup server on port 8080
WifiServer server(8080, PROTO_TCP);

void setup() {
  initAll();

  // Connect to AP
  Serial.println("Starting WiFi");
  Serial.println(wireless_prof.ip);
  Wireless.begin(&wireless_prof);
  Serial.println("Wifi Connected");

  // Start server
  /* server.begin(); */
  Serial.println("Started TCP server");
}

uint8_t read_byte(WifiClient client) {
  int in;
  while ((in = client.read()) == -1);
  return in;
}

float read_dist(WifiClient client) {
  return read_byte(client);
}

float read_angle(WifiClient client) {
  uint8_t angle = read_byte(client);
  return (float)angle * (360.0 / 256.0);
}

void loop() {
  WifiClient client = server.available();
  if (!client) return;

  while (client.connected()) {
    uint8_t cmd = read_byte(client);
    switch (cmd) {
    case 0:
      Serial.println("Forward");
      moveCm(read_dist(client));
      break;
    case 1:
      Serial.println("Backward");
      moveCm(-read_dist(client));
      break;
    case 2:
      Serial.println("Rotate Right");
      rotate(read_angle(client));
      break;
    case 3:
      Serial.println("Rotate Left");
      rotate(-read_angle(client));
      break;
    case 4: {
      unsigned long dist = sonarDetect();
      Serial.println("Distance: ");
      Serial.print(dist);
      client.write(dist & 0xFF);
      client.write(dist >> 8);
      break;
    }
    case 5: {
      Serial.print("Temperature: ");
      for (int i = 1; i <= 9; i++) {
        uint8_t temp = getTemp(i);
        Serial.print(temp);
        Serial.print(" ");
        client.write(temp);
      }
      Serial.println();
      break;
    }
    default:
      Serial.print("Unknown: ");
      Serial.println(cmd);
      break;
    }
  }
}
