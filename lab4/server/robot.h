#ifndef ROBOT_H
#define ROBOT_H

#include <stddef.h>


enum CommandType {
  MOVE,
  ROTATE,
  PAUSE
};

struct Command {
  CommandType type;
  float val;
};

/**
 * Initialize all the robot's hardware
 */
void initAll();

/**
 * Initializes the robot's serial monitor
 */
void initSerial();

/**
 * Initializes the robot's LCD
 */
void initLCD();

/**
 * Initializes the robot's LED
 */
void initLED();

/**
 * Initializes the robot's thermometer
 */
void initThermometer();

/**
 * Initializes the robot's motors
 */
void initMotors();

/**
 * Initializes the robot's motor sensors
 */
void initMotorSensors();

/**
 * Clears the LCD screen
 */
void clearScreen();

/**
 * Prints two strings centered to the LCD
 */
void printCentered(const char *first, const char *second);

/**
 * Prints the two student numbers for 5 seconds
 * and blinks the LED every second
 */
void printStudentNums(const char *n1, const char *n2);

int getTemp(int mode);

/**
 * Read the ambient temperature from the thermometer
 */
int getAmbTemp();

/**
 * Display ambient temperature on the LCD
 */
void displayAmbTemp();


unsigned long sonarDetect();

/**
 * Detects an obstruction that is within the provided distance
 *
 * @limit at what distance should the arduino bot detect an obstruction
 */
bool detectObstruction(unsigned int limit);

/**
 * Similar to function below but measures ditance in cm, not tiles
 * @cm Number of cem to move for 
 */
void moveCm(float cm);

/**
 * Move forward / backward
 *
 * @tiles Number of tiles to move for
 */
void move(float tiles);



bool moveAndCheck(int tiles, unsigned int distance);

/**
 * Rotate clock-wise / counter-clock-wise
 *
 * @param degrees Angle of rotation in degrees
 */
void rotate(float degrees);

/**
 * Pause the robot
 *
 * @param ms Number of seconds to stay paused for
 */
void pause(float seconds);

/**
 * Execute a series of commands
 *
 * @param path The list of commands to execute
 * @param len The number of commands to execute
 */
void traverse(const Command *path, size_t len);

/**
 * Execute a series of commands in reverse
 *
 * @param path The list of commands to execute
 * @param len The number of commands to execute
 */
void traverse_backwards(const Command *path, size_t len);

#endif
