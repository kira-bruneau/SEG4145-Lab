/*******************************************************************************
 *
 * Names:       Victor Diec, Kurt Bruneau
 * Course Code: SEG 4145
 * Lab Number:  1
 * Date:        Feb 1, 2016
 *
 * Description:
 * A program that prints our student numbers and traverses two paths
 *
 ******************************************************************************/

#include <SoftwareSerial.h>

#define ARRAY_LEN(x) (sizeof(x)/sizeof(*x))

/* Motor pins */
#define MOTOR_LEFT_PIN 45
#define MOTOR_RIGHT_PIN 8

/* Motor movement constants */
#define MOTOR_LEFT_FORWARD 245
#define MOTOR_RIGHT_FORWARD 138
#define MOTOR_LEFT_BACKWARD 160
#define MOTOR_RIGHT_BACKWARD 233

/* Motor rotation constants */
#define MOTOR_MILLIS_PER_DEGREE 7.5

enum CommandType {
  MOVE,
  ROTATE,
};

struct Command {
  CommandType type;
  int val;
};

const Command path1[] = {
  { MOVE, 2000 },
  { ROTATE, -90 },
  { MOVE, 2000 },
  { ROTATE, 90 },
  { MOVE, 2000 },
  { ROTATE, 90 },
  { MOVE, 2000 },
  { ROTATE, -90 },
  { MOVE, 2000 },
};

const Command path2[] = {
  { MOVE, 2000 },
  { ROTATE, 90 },
  { MOVE, 1000 },
  { ROTATE, -135 },
  { MOVE, 2828 },
  { ROTATE, 135 },
  { MOVE, 1000 },
  { ROTATE, -90 },
  { MOVE, 2000 },
};

/* Initialize the LCD */
SoftwareSerial lcd(0, 18);

/**
 * Execute an LCD command
 */
void lcdCommand(char command) {
  lcd.write(0xFE);
  lcd.write(command);
}

/**
 * Clears the LCD screen
 */
void clearScreen() {
  lcdCommand(0x01);
}

/**
 * Positions the cursor for the LCD
 */
void setCursor(char col, char row) {
  lcdCommand(col + row * 64 + 128);
}

/**
 * Prints two lines centered to the LCD
 */
void printCentered(const char *first, const char *second) {
  setCursor((16 - strlen(first)) / 2, 0);
  lcd.print(first);
  setCursor((16 - strlen(second)) / 2, 1);
  lcd.print(second);
}

/**
 * Prints the two student numbers
 */
void printStudentNums(const char *n1, const char *n2) {
  printCentered(n1, n2);

  // LED light blinking
  for (int i = 0; i < 5; i++) {
      digitalWrite(LED_BUILTIN, LOW);
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);
      delay(500);
  }

  digitalWrite(LED_BUILTIN, LOW);
  clearScreen();
}

/**
 * Move forward / backward
 *
 * @dist Number of milliseconds to move for
 */
void move(int dist) {
  printCentered("Moving", dist >= 0 ? "Forward" : "Backward");
  analogWrite(MOTOR_LEFT_PIN, dist >= 0 ? MOTOR_LEFT_FORWARD : MOTOR_LEFT_BACKWARD);
  analogWrite(MOTOR_RIGHT_PIN, dist >= 0 ? MOTOR_RIGHT_FORWARD : MOTOR_RIGHT_BACKWARD);
  delay(abs(dist));
  clearScreen();
}

/**
 * Rotate clock-wise / counter-clock-wise
 *
 * @param angle Angle of rotation in degrees
 */
void rotate(int angle) {
  printCentered("Rotating", angle >= 0 ? "Right" : "Left");
  analogWrite(MOTOR_LEFT_PIN, angle >= 0 ? MOTOR_LEFT_FORWARD : MOTOR_LEFT_BACKWARD);
  analogWrite(MOTOR_RIGHT_PIN, angle >= 0 ? MOTOR_RIGHT_BACKWARD : MOTOR_RIGHT_FORWARD);
  delay(MOTOR_MILLIS_PER_DEGREE * abs(angle));
  clearScreen();
}

/**
 * Stop all motors
 *
 * @param ms Number of milliseconds to stay stopped for
 */
void stop(int ms) {
  printCentered("Stopped", "");
  analogWrite(MOTOR_LEFT_PIN, 0);
  analogWrite(MOTOR_RIGHT_PIN, 0);
  delay(ms);
  clearScreen();
}

/**
 * Execute a series of commands
 *
 * @param path The list of commands to execute
 * @param len The number of commands to execute
 */
void traverse(const Command *path, size_t len) {
  for (size_t i = 0; i < len; i++) {
    switch (path[i].type) {
    case MOVE: move(path[i].val); break;
    case ROTATE: rotate(path[i].val); break;
    }
  }

  stop(5000);
}

/**
 * Execute a series of commands in reverse
 *
 * @param path The list of commands to execute
 * @param len The number of commands to execute
 */
void traverse_backwards(const Command *path, size_t len) {
  for (size_t i = len; i-- != 0 ;) {
    switch (path[i].type) {
    case MOVE: move(-path[i].val); break;
    case ROTATE: rotate(-path[i].val); break;
    }
  }

  stop(5000);
}

void setup() {
  lcd.begin(9600);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(MOTOR_LEFT_PIN, OUTPUT);
  pinMode(MOTOR_RIGHT_PIN, OUTPUT);

  clearScreen();

  // LCD is kind of buggy on battery
  printStudentNums("6901506", "7238982");

  printCentered("Path 1", "Forward");
  delay(3000);
  traverse(path1, ARRAY_LEN(path1));

  printCentered("Path 1", "Backward");
  delay(3000);
  traverse_backwards(path1, ARRAY_LEN(path1));

  printCentered("Path 2", "Forward");
  delay(3000);
  traverse(path2, ARRAY_LEN(path2));

  printCentered("Path 2", "Backward");
  delay(3000);
  traverse_backwards(path2, ARRAY_LEN(path2));
}

void loop() {
}